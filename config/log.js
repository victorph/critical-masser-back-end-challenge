'use strict';

const bunyan = require('bunyan');

const log = bunyan.createLogger( {
	name: 'Critical_Masser_Challenge.ScrapperService',
	src: true,
	serializers: { err: bunyan.stdSerializers.err },
	streams:
	[
		{ level: 'debug', stream: process.stdout },
		{ level: 'warn', stream: process.stderr },
		{ level: 'error', path: process.env.LOG_ERROR }
	]
} );

module.exports = log;