
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/critical_masser_test');

const FeedEntry = mongoose.model( 'FeedEntry', { title: String, link: String, data: Object, date: Date } );

module.exports = mongoose;