'use strict';

require('dotenv').load();

const async = require('async');

const moment = require('moment');

const schedule = require('node-schedule');

const log = require('./config').log;

const Scrapper = require('./scrapper.js');

const link = process.argv[ 2 ] || process.env.DEFAULT_LINK;

process.title = process.env.SCRAP_TITLE;

schedule.scheduleJob( process.env.SCRAP_FREQUENCY, () => {

	if ( process.env.NODE_ENV === 'development' ) { log.info('Running scrap'); }

	async.auto( {

		scrap: ( done ) => {
			return Scrapper.scrap( link, done );
		},

		last: [ 'scrap', ( res, done ) => {
			return Scrapper.getLast( done );
		} ],

		check: [ 'last', ( res, done ) => {

			if ( ! res.last )
			{
				return Scrapper.send( res.scrap, done );
			}

			let date_a = moment( new Date( res.last.publishedDate ) );

			let date_b = moment( new Date( res.scrap.entries[ 0 ].publishedDate ) );

			if ( date_a.diff( date_b ) > 0 )
			{

				if ( process.env.NODE_ENV === 'development' ) { log.info('Found new entry'); }

				// send to feed service after check if data has changed
				return Scrapper.send( {
					title: res.scrap.title,
					link: res.scrap.link,
					data: res.scrap.entries[ 0 ],
					date: new Date( res.scrap.entries[ 0 ].publishedDate ),
				}, done );

			}

		} ]

	}, ( err ) => {

		if ( err ) { return log.error( err ); }

		if ( process.env.NODE_ENV === 'development' )
		{
			log.info( 'Scrap task finished' );
		}

	} );

/*
	return Scrapper.scrap( link, ( err, data ) => {

		if ( err ) { return log.error( err ); }

		Scrapper.getLast( console.log );

		if ( ! check.last_item )
		{
			check.last_item = data.entries[ 0 ].publishedDate;
		}
		else
		{

			let date_a = moment( check.last_item );

			let date_b = moment( data.entries[ 0 ].publishedDate );

			if ( date_a.diff( date_b ) > 0 )
			{

				if ( process.env.NODE_ENV === 'development' ) { log.info('Got new news'); }

				// send to feed service after check if data has changed
				return Scrapper.send( {
					title: data.title,
					link: data.link,
					data: data.entries[ 0 ],
					date: new Date( data.entries[ 0 ].publishedDate ),
				}, ( err, res ) => {
					if ( err ) { return log.error( err ); }
					return;
				} );

			}

		}

	} );
*/

} );

