
# Critical Masser Back-end Developer Challenge #

Usually leave the .env out of the git repository but in this case I'm leaving it in as it contains various defaults

Command to start the scrapper:
    npm run-script scrapper

Command to start the feed service:
    npm run-script feed

Command to clean the tmp folder (logs folder):
    npm run-script clean

Command to start the tests:
    npm test

I recommend running the test scripts after the first run of the services, Since I'm using the real data
And not "mockups" for the tests, So the tests will fail without the data.
