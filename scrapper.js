'use strict';

require('dotenv').load();

const log = require('./config').log;

const request = require('request');

const FeedReader = require('feed-reader');

const util = require('util');

class Scrapper {

	static scrap ( link, cb ) {
		if ( ! link ) { return log.error('No link specified'); }
		if ( ! util.isFunction( cb ) ) { return log.error('Callback is required'); }
		return FeedReader.parse( link ).then( feed => {
			return cb( null, feed );
		} ).catch( error => {
			return cb( error );
		} );
	}

	static getLast ( cb ) {

		return request.get( {
			url: process.env.FEED_LINK
		}, (err, res, body) => {

			if ( err )
			{
				cb( err );
				return log.error( err );
			}

			if ( res.statusCode >= 400 )
			{
				cb( body );
				return log.error( body );
			}

			if ( process.env.NODE_ENV === 'development' )
			{
				log.info('Got feed items from database successfully');
			}

			const entries = JSON.parse( body );

			let last;

			if ( entries.length === 0 ) { last = null; }
			else { last = entries[ 0 ]; }

			cb( null, last );

		} );

	}

	static send ( data, cb ) {

		return request.post(
		{
			url: process.env.FEED_LINK,
			formData: { data: JSON.stringify( data ) }
		}, (err, res, body) => {

			if ( err )
			{
				cb( err );
				return log.error( err );
			}

			if ( res.statusCode >= 400 )
			{
				cb( body );
				return log.error( body );
			}

			if ( process.env.NODE_ENV === 'development' )
			{
				log.info('Data has been sent to feed API successfully');
			}

			cb( null, body );

		});

	}

}

module.exports = Scrapper;
