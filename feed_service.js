'use strict';

require('dotenv').load();

const async = require('async');

const config = require('./config');

const restify = require('restify');

const server = restify.createServer();

process.title = process.env.FEED_TITLE;

function saveEntries ( data, done ) {

	return async.each( data.entries, ( it, nxt ) => {

		let entry = new config.db.models
			.FeedEntry( {
				title: data.title,
				link: data.link,
				data: it,
				date: it.publishedDate
			} );

		entry.save( err => {
			if ( err )
			{
				config.log.error( err );
				return nxt( err );
			}
			return nxt();
		} );

		return;

	}, ( err ) => {

		if ( ! err && process.env.NODE_ENV === 'development' )
		{
			config.log.info('Saving completed');
		}

		return done( err );

	} );

}

server.use( restify.bodyParser( {
	maxBodySize: 0,
	mapParams: true,
	mapFiles: false,
	overrideParams: false
} ) );

server.get('/feed', (req, res, next) => {
	return config.db.models
		.FeedEntry
		.find()
		.limit( 20 )
		.sort( { date: -1 } )
		.exec( ( err, entries ) => {
			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return next();
			}
			res.json( entries );
			return next();
		} );
} );

server.get('/feed/:id', (req, res, next) => {
	return config.db.models
		.FeedEntry
		.findOne( { _id: req.params.id } )
		.exec( ( err, entry ) => {
			console.log( req.params.id );
			console.log( entry );
			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return next();
			}
			res.json( entry );
			return next();
		} );
} );

server.put('/feed/:id', (req, res, next) => {
	return config.db.models
		.findOneAndUpdate( { _id: req.params.id } )
		.exec( ( err, aff ) => {
			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return next();
			}
			res.json( aff );
			return next();
		} );
} );

server.del('/feed/:id', (req, res, next) => {
	return config.db.models
		.FeedEntry
		.remove( { _id: req.params.id } )
		.exec( ( err, aff ) => {
			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return next();
			}
			res.json( aff );
			return next();
		} );
} );

server.post('/feed', ( req, res, next ) => {

	req.body = JSON.parse( req.body.data );

	if ( req.body.entries )
	{
		// save all, because the API sent nothing to the scrapper
		saveEntries( req.body, ( err ) => {

			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return;
			}

			if ( process.env.NODE_ENV === 'development' )
			{
				config.log.info('All entries have been saved');
			}

			res.json( true );

			return next();

		} );

	}
	else
	{

		// save one, because there are entries in the DB
		let entry = new config.db.models.FeedEntry( {
			title: req.body.title,
			link: req.body.link,
			data: req.body.data,
			date: req.body.date
		} );

		entry.save( err => {

			if ( err )
			{
				config.log.error( err );
				res.json( err );
				return;
			}

			if ( process.env.NODE_ENV === 'development' )
			{
				config.log.info('Saving completed');
			}

			res.json( true );

			return next();

		} );

	}

	return;

} );

server.listen( process.env.PORT, () => {

	if ( process.env.NODE_ENV === 'development' )
	{
		config.log.info( `${server.name} listening at ${server.url}` );
	}

} );

