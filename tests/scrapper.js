'use strict';

require('dotenv').load();

const assert = require('chai').assert;

const Scrapper = require('./../scrapper.js');

process.env.NODE_ENV = 'test';

describe('Scrapper', () => {

	describe('#scrap()', () => {

		it('It should fetch the content of any given RSS feed', ( done ) => {

			Scrapper.scrap( process.env.DEFAULT_LINK, ( err, res ) => {
				if ( err ) { throw err; }
				if ( res.entries.length !== 25 ) { throw 'Wrong data'; }
				return done();
			} );

		} );

	} );

	describe('#getLast()', () => {

		it('It should fetch the last feed entry in the database', ( done ) => {
			Scrapper.getLast( ( err, last ) => {
				if ( err ) { throw err; }
				if ( ! last ) { throw 'No last item'; }
				return done();
			} );
		} );

	} );

	describe('#send()', () => {

		it('It should send the found data to the feed API service', ( done ) => {
			Scrapper.send( {
				title: 'Test',
				link: 'http://localhost',
				data: {},
				date: new Date(),
			} , ( err, entry ) => {
				if ( err ) { throw err; }
				if ( entry !== 'true' ) { throw 'Failed!'; }
				return done();
			} );
		} );

	} );

} );
