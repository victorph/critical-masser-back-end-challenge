'use strict';

const assert = require('chai').assert;

require('dotenv').load();

const request = require('request');

const Scrapper = require('./../scrapper.js');

process.env.NODE_ENV = 'test';

let entries;

describe('Feed', () => {

	describe('GET /feed', () => {

		it('It should return twenty feed entries', ( done ) => {

			return request.get( {
				url: process.env.FEED_LINK
			}, (err, res, body) => {
				if ( err ) { throw err; }
				if ( res.statusCode >= 400 ) { throw body; }
				entries = JSON.parse( body );
				if ( entries.length !== 20 ) { throw 'Not twenty entries'; }
				return done();
			} );

		} );

	} );

	describe('GET /feed/:id', () => {

		it('It should fetch an entry by id', ( done ) => {

			return request.get( {
				url: `${process.env.FEED_LINK}/${entries[ 0 ]._id}`
			}, (err, res, body) => {
				if ( err ) { throw err; }
				if ( res.statusCode >= 400 ) { throw body; }
				body = JSON.parse( body );
				if ( entries[ 0 ]._id !== body._id ) { throw 'Not equal ID'; }
				return done();
			} );

		} );

	} );

	describe('POST /feed', () => {

		it('It shoueld be able to create entries in the database', ( done ) => {

			return request.post( {
				url: process.env.FEED_LINK,
				formData:
				{
					data: JSON.stringify( {
						title: 'Test',
						link: 'http://localhost',
						data: {},
						date: new Date(),
					} )
				}
			}, (err, res, body) => {
				if ( err ) { throw err; }
				if ( res.statusCode >= 400 ) { throw body; }
				if ( err ) { throw err; }
				if ( body !== 'true' ) { throw 'Failed!'; }
				return done();
			} );

		} );

	} );

} );
